import { Component } from '@angular/core';
import { SQLite, SQLiteObject } from "@ionic-native/sqlite/ngx";

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  data: any = [];

  constructor(
    public sqlite: SQLite,
  ) { }

  ionViewDidEnter(event) {

    if (!event) {
      event = ''
    }
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      let query = 'CREATE TABLE IF NOT EXISTS pegawai( id INTEGER PRIMARY KEY, nama, nik, jk, tgl_lahir )';
      db.executeSql(query, []).then(() => { console.log('new table created'); }).catch(e => console.error(e));

      let query_data: any = "SELECT * FROM pegawai WHERE nama LIKE ?";
      db.executeSql(query_data, ['%' + event + '%']).then((data) => {
        console.log('list data showed');

        this.data = [];

        if (data.rows.length > 0) {
          for (let i = 0; i < data.rows.length; i++) {
            this.data.push(data.rows.item(i));
          }
        }
      }).catch(e => console.error(e));
    }).catch(e => console.error(e));
  }

}
