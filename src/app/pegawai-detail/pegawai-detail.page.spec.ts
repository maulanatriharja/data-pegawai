import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PegawaiDetailPage } from './pegawai-detail.page';

describe('PegawaiDetailPage', () => {
  let component: PegawaiDetailPage;
  let fixture: ComponentFixture<PegawaiDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PegawaiDetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PegawaiDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
