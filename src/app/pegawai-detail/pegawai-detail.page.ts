import { AlertController, ToastController } from '@ionic/angular';
import { SQLite, SQLiteObject } from "@ionic-native/sqlite/ngx";
import { FormGroup, FormBuilder } from "@angular/forms";
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from "@angular/common";

@Component({
  selector: 'app-pegawai-detail',
  templateUrl: './pegawai-detail.page.html',
  styleUrls: ['./pegawai-detail.page.scss'],
})
export class PegawaiDetailPage implements OnInit {

  id: string = this.route.snapshot.paramMap.get('id');

  myForm: FormGroup;
  isSubmitted = false;

  constructor(
    public alertController: AlertController,
    public formBuilder: FormBuilder,
    public location: Location,
    public route: ActivatedRoute,
    public sqlite: SQLite,
    public toastController: ToastController,
  ) {
    this.myForm = this.formBuilder.group({
      nama: [''],
      nik: [''],
      jk: [''],
      tgl_lahir: [''],
    });
  }

  ngOnInit() {
    if (this.id) {
      this.sqlite.create({
        name: 'data.db',
        location: 'default'
      }).then((db: SQLiteObject) => {
        let query: any = "SELECT * FROM pegawai WHERE id=?";
        db.executeSql(query, [this.id]).then((data) => {
          console.log('list data showed');

          this.myForm = this.formBuilder.group({
            nama: [data.rows.item(0).nama],
            nik: [data.rows.item(0).nik],
            jk: [data.rows.item(0).jk],
            tgl_lahir: [data.rows.item(0).tgl_lahir],
          });

        }).catch(e => console.error(e));
      }).catch(e => console.error(e));
    }
  }

  submit() {
    this.isSubmitted = true;

    if (this.myForm.valid) {
      if (this.id) {
        this.update();
      } else {
        this.simpan();
      }
    }
  }

  simpan() {
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      let query: any = 'INSERT INTO pegawai ( nama, nik, jk, tgl_lahir ) VALUES ( ?, ?, ?, ?)';
      db.executeSql(query, [
        this.myForm.get('nama').value,
        this.myForm.get('nik').value,
        this.myForm.get('jk').value,
        this.myForm.get('tgl_lahir').value.slice(0, 10)
      ]).then(async () => {
        console.log('new data inserted');

        const toast = await this.toastController.create({
          message: 'Berhasil menambah data baru.',
          position: 'top',
          color: 'success',
          duration: 2000,
          buttons: [
            {
              text: 'X',
              role: 'cancel',
              handler: () => {
                console.log('Cancel clicked');
              }
            }
          ]
        });
        toast.present();

        this.location.back();
      }).catch(e => console.error(e));
    }).catch(e => console.error(e));
  }

  update() {
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      let query: any = 'UPDATE pegawai SET nama=?, nik=?, jk=?, tgl_lahir=? WHERE id=?';
      db.executeSql(query, [
        this.myForm.get('nama').value,
        this.myForm.get('nik').value,
        this.myForm.get('jk').value,
        this.myForm.get('tgl_lahir').value.slice(0, 10),
        this.id
      ]).then(async () => {
        console.log('data updated');

        const toast = await this.toastController.create({
          message: 'Berhasil mengubah data.',
          position: 'top',
          color: 'success',
          duration: 2000,
          buttons: [
            {
              text: 'X',
              role: 'cancel',
              handler: () => {
                console.log('Cancel clicked');
              }
            }
          ]
        });
        toast.present();

        this.location.back();
      }).catch(e => console.error(e));
    }).catch(e => console.error(e));
  }

  async hapus() {
    const alert = await this.alertController.create({
      header: 'Konfirmasi',
      message: 'Yakin untuk menghapus data ?',
      buttons: [
        {
          text: 'Batal',
          role: 'cancel',
        }, {
          text: 'Hapus',
          handler: () => {
            this.sqlite.create({
              name: 'data.db',
              location: 'default'
            }).then((db: SQLiteObject) => {
              let query: any = 'DELETE FROM pegawai WHERE id=?';
              db.executeSql(query, [this.id]).then(async () => {
                console.log('data deleted');

                const toast = await this.toastController.create({
                  message: 'Berhasil menghapus data.',
                  position: 'top',
                  color: 'medium',
                  duration: 2000,
                  buttons: [
                    {
                      text: 'X',
                      role: 'cancel',
                      handler: () => {
                        console.log('Cancel clicked');
                      }
                    }
                  ]
                });
                toast.present();

                this.location.back();
              }).catch(e => console.error(e));
            }).catch(e => console.error(e));
          }
        }
      ]
    });
    await alert.present();
  }
}