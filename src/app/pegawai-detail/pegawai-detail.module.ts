import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PegawaiDetailPageRoutingModule } from './pegawai-detail-routing.module';

import { PegawaiDetailPage } from './pegawai-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    PegawaiDetailPageRoutingModule
  ],
  declarations: [PegawaiDetailPage]
})
export class PegawaiDetailPageModule {}
