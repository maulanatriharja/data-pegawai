import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PegawaiDetailPage } from './pegawai-detail.page';

const routes: Routes = [
  {
    path: '',
    component: PegawaiDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PegawaiDetailPageRoutingModule {}
